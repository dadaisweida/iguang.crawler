<?php
$arr = array(
	mode => "2",
	0 => array(
		0 => array(
			"level"=>"15",
			"url"=>"http://shopping.pchome.com.tw",
			"cate"=>"http://shopping.pchome.com.tw",
			"range"=>"/<div id=\"site_nav1\" class=\"site_nav\">.*?book(.*?)<\/div>/si",
			"pattern"=>"/http:\/\/24h.pchome.com.tw\/\?m=index&f=view&p=24hour&s=(.*?)\"/si",
			"combine"=>"http://shopping.pchome.com.tw/?mod=area&func=style_show&RG_NO=DMAA&BB=",
			"stop"=>1,
			"shop"=>"48",
			"cate_name"=>"/(?|((.*)))/si",
			"parent"=>"0",
			"title_pattern"=>"/(?|(http:\/\/24h.pchome.com.tw\/\?m=.*?>(.*?)<))/si"),
		1 => array(
			"level"=>"2",
			"url"=>"http://shopping.pchome.com.tw",
			"cate"=>"http://shopping.pchome.com.tw/?mod=area&func=style_show&RG_NO=DMAA&BB=cp",
			"range"=>"",
			"pattern"=>"/nowrap><a href=\"(.*?)\"/si",
			"combine"=>"http://shopping.pchome.com.tw",
			"stop"=>0,
			"shop"=>"48",
			"cate_name"=>"/(?|(NO=(.*?)&)|(NO=(.*?)$))/si",
			"parent"=>"cp",
			"title_pattern"=>"/(?|(nowrap><a href=.*?onmouseover=.*?=\'(.*?)\'))/si"),
		2 => array(
			"level"=>"3",
			"url"=>"http://shopping.pchome.com.tw",
			"cate"=>"http://shopping.pchome.com.tw",
			"range"=>"/<table id=\"tbl1\"(.*?)<\/style>/si",
			"pattern"=>"/(?|(<a href=\"\/\?(.*?)\")|(<a href=\"\?(.*?)\"))/si",
			"combine"=>"http://shopping.pchome.com.tw/?",
			"stop"=>1,
			"shop"=>"48",
			"cate_name"=>"/(?|(NO=(.*?)&)|(NO=(.*?)$))/si",
			"parent"=>"3c",
			"title_pattern"=>"/(?|(<a href=\"\/\?.*?color=.*?>(.*?)<)|(<a href=\"\?.*?color=.*?>(.*?)<))/sis")
		));

$json = json_encode($arr);

print_r($json);
?>