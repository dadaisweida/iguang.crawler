<?php
	class WriteJSON{
		function jload_pageurl($prod,$data){ // 輸出所有分頁 URL
			echo "<br><br>----------------------------讀取所有分頁----------------------------<br>";

			$page_pattern = $prod->page_pattern;
			$page_range = $prod ->page_range;
			if($page_range){
				preg_match($page_range,$data,$taglist);
				$data=$taglist[2];
			}
			$paginate = $prod->paginate;
			switch($paginate){
				case "url":
					echo "解析分頁方式".$paginate;
					$array_page_url[] = clone $prod;
					echo "<br>解析分頁網址: ".$prod->site."<br>";
					$page=$prod->site;
					preg_match_all($page_pattern,$data,$taglist_page);
					// for books magzine
					if(strpos($taglist_page[1][0],"avascript")!==false){
						$n1=strrpos($taglist_page[1][0],",");
						$n2=strpos($taglist_page[1][0],";");
						$taglist_page[1][0]=substr($taglist_page[1][0],$n1+1,$n2-$n1-2);
					}
					// normal
					if(strpos($taglist_page[1][0],"ttp")!==false||$prod->shop==39)
					{
						echo "分頁數目: ".count($taglist_page[1])."<br>";
						$i=0;
						if(count($taglist_page[1]) > 0){
							foreach($taglist_page[1] as $tagTmp){
								$temp_sp = $prod->page_url;
								$array_page[$i] = $temp_sp.$tagTmp;
								echo $i."分頁連結: ".$array_page[$i].$url."<br>";
								$prod->site = $array_page[$i].$url;
								$array_page_url[] = clone $prod;
								$i++;
							}
						}
					}
					else{ // 共有幾頁
						$i=0;
						$count=$taglist_page[1][0];
						if($count=='')
							$count=0;
						echo "分頁數目: ".$count."<br>";
						for($x=2;$x<=$count;$x++){
							$array_page[$i]=$page;
							$prod->site = $array_page[$i].$url."&page=".$x;
							echo $i."分頁連結: ".$prod->site."<br>";
							$array_page_url[] = clone $prod;
							$i++;
						}
					}
					break;
				case "pagen":
					echo "解析分頁方式".$paginate;
					switch($prod->shop){
						default: // cite, pchome
							$prod->site = $prod->site."&page=1";
							break;
					}
					$array_page_url[] = clone $prod;
					echo "<br>解析分頁網址: ".$prod->site."<br>";
					preg_match_all($page_pattern,$data,$taglist_pagen);
					if($taglist_pagen[1][0]=="")
						$taglist_pagen[1][0]=0;
					echo "分頁數目: ".$taglist_pagen[1][0]."<br>";
					for($i=2 ; $i<= $taglist_pagen[1][0] ; $i++){
						switch ($prod->shop) {
							default: // cite, pchome
								$prod->site = preg_replace('/\&page=.*/si','&page='.$i,$prod->site);
								break;
						}
						$array_page_url[] = clone $prod;
					}
					break;
				case "prodn":
					echo "解析分頁方式".$paginate;
					switch($prod->shop){
						case "ebay-us":
						case "ebay-uk":
						case "ebay-au":
							$temp = $prod->site;
							break;
						default: // 1, 2, 18, 40
							preg_match($prod->fix_pattern, $prod->site, $taglist_fix);
							$prod->site = $prod->page_combine;
							$arr = array('-replace-' => $taglist_fix[2]);
							$prod->site = strtr($prod->site,$arr);
							$tempurl = $prod->site;
							break;
					}
					echo "<br>解析分頁網址: ".$prod->site."<br>";
					preg_match($prod->page_pattern,$data,$taglist_page);
					$taglist_page[1] = strip_tags($taglist_page[1]); // 去除HTML標籤，取得商品數目
					$onepagen = $prod->onepage_n;
					$i=0;
					if(count($taglist_page[1]) > 0){
						if(strpos($taglist_page[1],',')!=0){ // 去除逗號
							$taglist_page[1]=str_replace(',', '', $taglist_page[1]);
						}
						$num=(int)$taglist_page[1];
						$count=ceil($num/$onepagen);
						echo "分頁數目: ".$count."<br>";
						for($i=0;$i<$count;$i++){
							switch($prod->shop){
								case "ebay-us":
								case "ebay-uk":
								case "ebay-au":
									$prod->site = $temp."&_pgn=".($i+1)."&_skc=".($i*50);
									break;
								case "2":
									$prod->site = preg_replace('/count=.*/si','count='.$i*1000,$prod->site);
									break;
								default: // 1, 18, 40
									$prod->site = preg_replace('/-replace2-/si',($i+1),$tempurl);
									break;
							}
							$array_page_url[] = clone $prod;
						}
					}
					break;
				default:
					echo "沒有分頁，或資料庫中沒有設定paginate欄位";
					if($prod->shop == "14"){
						$prod->site = preg_replace('/http(.*?)\?/si','http://www.taaze.tw/beta/viewDataAgent.jsp?endNum=10000&',$prod->site);
					}
					$array_page_url[] = clone $prod;
					echo "<br>解析分頁網址: ".$prod->url."<br>";
					break;
			}
			return $array_page_url;
		}
		function jload_produrl($prod,$data,$db){ // 輸出所有 product URL
			$wd = new WriteDb;
			$array_item = array();
			preg_match_all($prod->prod_pattern,$data,$taglist_item);
			if($taglist_item[2]==""){
				$index=1;
			}
			else $index=2;
			echo "<br>商品數: ".count($taglist_item[$index])."<br>來源: ".$prod->site."<br>";
			$i=0;
			if(count($taglist_item[$index]) > 0){
				foreach($taglist_item[$index] as $tagTmp){
					$temp_sp = $prod->prod_url;
					$array_temp = $temp_sp.$tagTmp;
					$arr = array('\\' => '');
					$array_temp = strtr($array_temp,$arr);
					array_push($array_item, $array_temp);
					echo $i.": ".$array_temp."<br>";
					$i++;
				}
			}
			$wd->jwrite_prod_url($db,$prod,$array_item); // 寫入資料庫
			return $array_item;
		}
		function jwfile_prod($prod,$data,$db){ // 書出所有 product information
			$wd = new WriteDb;
			$data_ld=str_replace(array("\r\n","\n","\t","  ","&nbsp;"),array("","","","",""),$data);
			$result=preg_match($prod->gdid, $data, $taglist);
			$result_img=preg_match($prod->imgurl, $data, $taglist_img);
			$result_title=preg_match($prod->title, $data, $taglist_title);
			$result_dprice=preg_match($prod->price, $data, $taglist_dprice);
			$result_source_page=$prod->domain_url;
			$result_Ldesc=preg_match($prod->desc, $data_ld, $taglist_Ldesc);
			if($prod->originalprice!="NULL")
				$result_price=preg_match($prod->originalprice, $data, $taglist_price);
			if($prod->buyer_num!="NULL")
				$result_buyn=preg_match($prod->buyer_num, $data, $taglist_buyn);
			if($prod->isbn!="NULL")
				$result_isbn=preg_match($prod->isbn, $data, $taglist_isbn);
			if($prod->author!="NULL")
				$result_author=preg_match($prod->author, $data, $taglist_author);
			if($prod->publish!="NULL")
				$result_publish=preg_match($prod->publish, $data, $taglist_publish);
			if($prod->save!="NULL")
				$result_save=preg_match($prod->save, $data, $taglist_save);

			$shop=$prod->shop;
			$source_page = $prod->url;
			$prod_url = $prod->prod_url;
			$parents=$prod->parents;
			echo "來源: ".$source_page."<br>";
			echo "網址: ".$prod_url."<br>";
			$end_tag = 1;
			for ($i=0;$i<$end_tag;$i++) {
				$tagTemp_gdid = $taglist[2];
				$tagTemp_gdid = strip_tags($tagTemp_gdid); //去除HTML標籤
				$tagTemp_gdid = str_replace(array("\r\n","\n","\t","  ","&nbsp;"),array("","","","",""),$tagTemp_gdid);
				$array_gdid[$i] = $tagTemp_gdid;
				echo $i."gid: ".$array_gdid[$i]."<br>";
				if($array_gdid[$i]!="")
					$wd->write_prod_cates($db,$shop,$array_gdid[$i],$parents,1);
				else
					$wd->write_prod_cates($db,$shop,$array_gdid[$i],$parents,0);
				$tagTmp_img = $taglist_img[2];
				$arr = array('<img data-original="' => '', '<img src="' => '', '<a href="' => '', '"' => '');
				$tagTmp_img = strtr($tagTmp_img,$arr);
				$array_img[$i] = $tagTmp_img;
				echo $i."img: ".$array_img[$i]."<br>";

				$tagTmp_title = $taglist_title[2];
				$tagTmp_title = strip_tags($tagTmp_title); //去除HTML標籤
				$tagTmp_title = str_replace(array("\r\n","\n","\t","  ","&nbsp;"),array("","","","",""),$tagTmp_title);
				$array_title[$i]=$tagTmp_title;
				echo $i."title: ".$array_title[$i]."<br>";

				$tagTmp_dprice = $taglist_dprice[2];
				$tagTmp_dprice = strip_tags($tagTmp_dprice); // 去除HTML標籤
				$tagTmp_dprice = str_replace(array("\r\n","\n","\t","  ","&nbsp;","$"),array("","","","","",""),$tagTmp_dprice);
				$array_dprice[$i]=$tagTmp_dprice;
				echo $i."dprice: ".$array_dprice[$i]."<br>";

				$tagTmp_price = $taglist_price[2]; // 原價
				$tagTmp_price = strip_tags($tagTmp_price); // 去除HTML標籤
				$tagTmp_price = trim($tagTmp_price); // 去除頭尾空白
				$arr = array(',' => ''); // 去除逗號
				$tagTmp_price = strtr($tagTmp_price,$arr); // strtr效率比preg_replace快四倍
				if($prod->save=="NULL"){
					$array_price[$i]=$tagTmp_price;
					echo $i."price: ".$array_price[$i]."<br>";
				}
				else{
					$array_price[$i]=$array_dprice[$i]+$taglist_save[2];
					echo $i."price: ".$array_price[$i]."<br>";
				}

				$tagTmp_buyn = $taglist_buyn[2];
				$tagTmp_buyn = strip_tags($tagTmp_buyn); // 去除HTML標籤
				$array_buyn[$i]=$tagTmp_buyn;
				echo $i."buyn: ".$array_buyn[$i]."<br>";

				$tagTmp_isbn = $taglist_isbn[2];
				$tagTmp_isbn = strip_tags($tagTmp_isbn); // 去除HTML標籤
				$array_isbn[$i]=$tagTmp_isbn;
				echo $i."isbn: ".$array_isbn[$i]."<br>";

				$tagTmp_author = $taglist_author[2];
				$tagTmp_author = strip_tags($tagTmp_author); // 去除HTML標籤
				$array_author[$i]=$tagTmp_author;
				echo $i."author: ".$array_author[$i]."<br>";

				$tagTmp_publish = $taglist_publish[2];
				$tagTmp_publish = strip_tags($tagTmp_publish); // 去除HTML標籤
				$array_publish[$i]=$tagTmp_publish;
				echo $i."publish: ".$array_publish[$i]."<br>";

				$tagTmp_Ldesc = $taglist_Ldesc[2];
				if ($prod->exception=="iframe") { // 目前適用momo919
					$temp_url = $result_source_page.$tagTmp_Ldesc;
					$temp_curl = new Curl;
					$array_Ldesc[$i]=$temp_curl->SingleCurl($temp_url);
					$array_Ldesc[$i] = preg_replace ('/src=\"\//', 'src="'.$result_source_page.'/', $array_Ldesc[$i]); // 修正短網址
				}
				else{
					//$arr = array('img src="/' => 'img src="http://i.i99.to/', 'data-original="/' => 'img src="http://i.i99.to/');
					//$tagTmp_Ldesc = strtr($tagTmp_Ldesc,$arr); // strtr效率比preg_replace快四倍
					$arr = array('src="/res' => 'src="'.$result_source_page, 'data-original="/' => 'img src="http://i.i99.to/');
					$tagTmp_Ldesc = strtr($tagTmp_Ldesc,$arr); // strtr效率比preg_replace快四倍
					$arr = array('data-original' => 'src');
					$tagTmp_Ldesc = strtr($tagTmp_Ldesc,$arr); // strtr效率比preg_replace快四倍
					$tagTmp_Ldesc = preg_replace('/^.+\n.+\n/', '', $tagTmp_Ldesc); // 去除前兩行
					$array_Ldesc[$i]=$tagTmp_Ldesc;
				}
				echo $i."Desc: ".$array_Ldesc[$i]."<br>";

				$data_arr=array(
					"gdid"=>$array_gdid[$i], "desc"=>$array_Ldesc[$i], "url"=>$prod_url
					, "price"=>$array_dprice[$i], "title"=>$array_title[$i], "endtime"=>"321"
					, "starttime"=>"321", "imgurl"=>$array_img[$i], "buyprod"=>"321"
					, "shop"=>$shop, "originalprice"=>$array_price[$i], "tag"=>"321"
					, "buyer_num"=>$array_buyn[$i], "update"=>"321", "sub_class"=>"321"
					, "code_server"=>"321", "timestamp"=>"321", "domain"=>"tw"
					, "ExecType"=>"prod", "isbn"=>$array_isbn[$i], "author"=>$array_author[$i]
					, "publish"=>$array_publish[$i]);
				$wd->jwrite_prod_info($db,$data_arr);
				$i++;
			}
			return $data_arr;
		}
	}
?>