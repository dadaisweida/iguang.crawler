<?php
	require_once "crawl.php";
	require_once "Curl.php";
	require_once "callapi.php";
	require_once "WriteDb.php";
	require_once "db_yw.php";
	require_once "WriteJSON.php";
	require_once "readDB.php";

	$start = microtime(true);
	set_time_limit(0);
	ini_set("memory_limit","1000M"); // 設定memory容量
	header("Content-Type:text/html; charset=utf-8");
	$crawl=new Std_crawl;
	$wd=new WriteDb;
	$read=new readDB;
	//$key = md5(gethostbyname(getHostName()));
	$key = gethostbyname(getHostName());
	$arr = array('.' => '');
	$key = strtr($key,$arr);
	$clients = "client-1";
	$read -> refresh_queue_step1($db, $key, $clients);

	while(TRUE){
		$json2 = $read->read_task_step1($db,$key);
		$json2 = $json2[0];
		$index1 = "0";
		$mode = $json2->mode;
		$cate_shop = $json2->$index1->$index1->shop;
		echo $cate_shop;
		try{
			switch($mode){
				case 1: // 解析 layer 1
					echo "mode: 1";
					$funtype = 'Digging_level1';
					$wd->reset_flag($db,$cate_shop);
					break;
				case 2: // 解析 layer 2 之後
					echo "mode: 2";
					$funtype = 'Digging';
					break;
				case 99:
					echo "server recall null, exit.";
					exit(0);
					break;
				default:
					echo "wait...";
					sleep(20);
					continue;
					break;
			}
			$array_cate = $crawl->cate($json2->$index1);
			foreach($array_cate as $a){
				if($a['level'] == 1||$a['level'] == 2){
					$crawl->$funtype($db,$a['shop'],$a['cate'],$a['parent'],$a,$array_cate);
				}
			}
		}
		catch (Exception $e) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
		$stop=microtime(true);
		$read->done_task_step1($db,$key);
		echo "<br><br>總共花費:".round($stop-$start,2)."sec";
		sleep(1);
	}
?>