<?php
	require_once "crawl.php";
	require_once "Curl.php";
	require_once "callapi.php";
	require_once "WriteDb.php";
	require_once "db_yw.php";
	require_once "WriteJSON.php";
	require_once "readDB.php";

	$start = microtime(true);
	set_time_limit(0);
	ini_set("memory_limit","1000M"); // 設定memory容量
	header("Content-Type:text/html; charset=utf-8");
	$ca=new Call_Api;
	$crawl=new Std_crawl;
	$read=new readDB;
	//$key = md5(gethostbyname(getHostName()));
	$key = gethostbyname(getHostName());
	$arr = array('.' => '');
	$key = strtr($key,$arr);
	$clients = "client-1";

	$exec_mode = 3;
	switch($exec_mode){
		case 2:
			$exec_type = "read_task_step2";
			$done_type = "done_task_step2";
			$read -> refresh_queue_step2($db, $key, $clients);
			break;
		case 3:
			$exec_type = "read_task_step3";
			$done_type = "done_task_step2";
			$read -> refresh_queue_step3($db, $key, $clients);
			break;
	}

	while(TRUE){
		//$json2 = json_decode(file_get_contents('http://127.0.0.1/iguang.crawler_readdb/data4.php'));
		$json2 = $read->$exec_type($db,$key);
		$json2 = $json2[0];
		$index = "0";
		$mode = $json2->mode;
		try{
			switch($mode){
				case 2: // 解product url
					$source_product_acum=$crawl->load($json2->$index);
					$total_array_pageurl=$crawl->page($source_product_acum,$db);
					$pageurl_acum=$crawl->load($total_array_pageurl);
					$total_array_prod_url=$crawl->item($pageurl_acum,$db);
					echo "<br><br>共: ".count($total_array_prod_url);
					break;
				case 3: // 解product info
					$source_product_acum=$crawl->load($json2->$index);
					$total_array_prod = $crawl->product($source_product_acum,$db);
					echo "丟進API之陣列大小: ".count($total_array_prod);
					break;
				case 99:
					echo "server recall null, exit.";
					exit(1);
					break;
				default:
					echo "wait...";
					$read -> renew_queue_step3($db, $key, $clients);
					sleep(20); // 等待十分鐘，再要一次
					continue;
					break;
			}
		}
		catch (Exception $e) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
		$stop=microtime(true);
		$read->$done_type($db,$key);
		echo "<br><br>總共花費:".round($stop-$start,2)."sec";

		sleep(1);
	}
?>