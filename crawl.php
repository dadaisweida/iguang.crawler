<?php
	abstract class crawl {
		public function cate($array) { // 解析 json
			$catejs = array();
			$i = 0;
			foreach($array as $tempcate){
				$temp[$i]['level']=$tempcate->level;
				$temp[$i]['cate']=$tempcate->cate;
				$temp[$i]['url']=$tempcate->url;
				$temp[$i]['range']=$tempcate->range;
				$temp[$i]['pattern']=$tempcate->pattern;
				$temp[$i]['combine']=$tempcate->combine;
				$temp[$i]['stop']=$tempcate->stop;
				$temp[$i]['shop']=$tempcate->shop;
				$temp[$i]['title_pattern']=$tempcate->title_pattern;
				$temp[$i]['parent']=$tempcate->parent;
				$temp[$i]['parents']=$tempcate->parent;
				$temp[$i++]['cate_name']=$tempcate->cate_name;
			}
			return $temp;
		}
		public function load($array) { // chunk
			$acum = array();
			$i = 0;
			$j = 0;
			foreach($array as $tmp){
				if($j < 10){
					$acum[$i][$j] = $tmp;
					$j++;
				}
				else if($j >= 10){
					$j = 0;
					$i++;
					$acum[$i][$j] = $tmp;
					$j++;
				}
			}
			return $acum;
		}
		public function page($source_product_acum,$db) {
			$w = new WriteJSON;
			$curls_produrl=new Curl;
			foreach($source_product_acum as $source_product_acum_2){
				$curl_produrl=$curls_produrl->jMultiCurl_url($source_product_acum_2);
				foreach($curl_produrl as $i => $ch) {
					$content_prod  = curl_multi_getcontent($ch['ch']); //若MultiCurl一次丟入量太多，會完全取不到content($ch[ch]資源被釋放)
					// 編碼轉換
					$encode_prod = mb_detect_encoding($content_prod, array('ASCII','BIG5','GB2312','GBK','UTF-8')); //判斷網頁編碼
					if(stripos($ch['prod']->shop, "ebay")===false){
					//if($ch[prod]->shop!="ebay-us" && $ch[prod]->shop!="ebay-uk" && $ch[prod]->shop!="ebay-au"){ // ebay不需要轉換編碼，否則會錯
						if($encode_prod!="UTF-8"){
							$content_prod = mb_convert_encoding($content_prod, 'UTF-8', $encode_prod); //轉換編碼
						}
					}
					$data_prod = (curl_errno($ch['ch']) == 0) ? $content_prod : false; // 判斷content_prod是否有值
					$array_prod=$w->jload_pageurl($ch['prod'],$data_prod);
					$total_array_pageurl = (object) array_merge((array) $total_array_pageurl, (array) $array_prod);
				}
			}
			unset($curls_produrl);
			unset($w);
			return $total_array_pageurl;
		}

		public function item($pageurl_acum,$db) {
			$w = new WriteJSON;
			$curls_produrl=new Curl;
			$total_array_prod = array();
			foreach($pageurl_acum as $pageurl_acum_2){
				$curl_produrl=$curls_produrl->jMultiCurl_url($pageurl_acum_2);
				foreach($curl_produrl as $i => $ch) {
					$content_prod  = curl_multi_getcontent($ch['ch']); //若MultiCurl一次丟入量太多，會完全取不到content($ch[ch]資源被釋放)
					// 編碼轉換
					$encode_prod = mb_detect_encoding($content_prod, array('ASCII','BIG5','GB2312','GBK','UTF-8')); //判斷網頁編碼
					if(stripos($ch['prod']->shop, "ebay")===false){
					//if($ch[prod]->shop!="ebay-us" && $ch[prod]->shop!="ebay-uk" && $ch[prod]->shop!="ebay-au"){ // ebay不需要轉換編碼，否則會錯
						if($encode_prod!="UTF-8"){
							$content_prod = mb_convert_encoding($content_prod, 'UTF-8', $encode_prod); //轉換編碼
						}
					}
					$data_prod = (curl_errno($ch['ch']) == 0) ? $content_prod : false; // 判斷content_prod是否有值
					$array_prod=$w->jload_produrl($ch['prod'],$data_prod,$db);
					$total_array_prod = array_merge($total_array_prod,$array_prod);
				}
			}
			unset($curls_produrl);
			unset($w);
			return $total_array_prod;
		}
		public function product($source_product_acum,$db) {
			$curls_prod=new Curl;
			$w=new WriteJSON;
			$array_prod=array();

			$i=0;
			echo "<br>count array acum".count($source_product_acum);
			foreach($source_product_acum as $source_product_acum_2){ // 每次丟入幾個object給multi_curl
				$w = new WriteJSON;
				$curl_prod=$curls_prod->jMultiCurl($source_product_acum_2);
				$total_array_prod = array();
				foreach($curl_prod as $i => $ch) {
					$content_prod  = curl_multi_getcontent($ch[ch]); //若MultiCurl一次丟入量太多，會完全取不到content($ch[ch]資源被釋放)
					// 編碼轉換
					$encode_prod = mb_detect_encoding($content_prod, array('ASCII','BIG5','GB2312','GBK','UTF-8')); //判斷網頁編碼
					if(stripos($ch['prod']->shop, "ebay")===false){
					//if($ch[prod]->shop!="ebay-us" && $ch[prod]->shop!="ebay-uk" && $ch[prod]->shop!="ebay-au"){ // ebay不需要轉換編碼，否則會錯
						if($encode_prod!="UTF-8"){
							$content_prod = mb_convert_encoding($content_prod, 'UTF-8', $encode_prod); //轉換編碼
						}
					}
					$data_prod = (curl_errno($ch['ch']) == 0) ? $content_prod : false; // 判斷content_prod是否有值
					$array_prod=$w->jwfile_prod($ch['prod'],$data_prod,$db);
					array_push($total_array_prod, $array_prod);
				}
			}
			unset($curls_produrl);
			unset($w);
			return $total_array_prod;
		}
		public function api($data_products)
		{
			// $i=0;
			// $ca=new callapi;
			// foreach ($data_products as $total_prod) {
			//	echo "丟進API之陣列大小: ".count($total_prod);
			//	$ca->call_api($total_prod);
			// }
			// unset($i);
			// unset($ca);
		}
		abstract public function Digging($db,$shop,$cate,$parent,$array1,$array);
	}
	class Std_crawl extends crawl {
		public function Digging($db,$shop,$cate,$parent,$array1,$array){
			$crawl=new Std_crawl ;
			foreach($array as $a){
				$son=$a['level']-$array1['level'];
				if($a['shop']==$shop && $son==1){
					$array2=$a;
				}
			}
			$w=new WriteDb;
			$curl=new Curl;
			$data=$curl->SingleCurl($cate);

			if($shop=="48"||$shop=="5"){//pchome
				$data = mb_convert_encoding($data, 'UTF-8', 'BIG5'); //轉換編碼
				$data = mb_convert_encoding($data, 'UTF-8', "UTF-8"); //轉換編碼
			}

			if(!empty($array1['range'])){
				preg_match($array1['range'],$data,$data1);
				preg_match_all($array1['pattern'],$data1[1],$temp);
				preg_match_all($array1['title_pattern'],$data1[1],$temp_title);
			}
			else{
				preg_match_all($array1['pattern'],$data,$temp);
				preg_match_all($array1['title_pattern'],$data,$temp_title);
			}

			if($temp[2]==""){
				$index=1;
			}
			else $index=2;

			if($array1['stop']=="1"){
				if(!empty($temp[$index])){
					$i=0;
					foreach($temp[$index] as $t){
						$title = $temp_title[2][$i++];
						$title = strip_tags($title); //去除HTML標籤
						$title = str_replace(array("\r\n","\n","\t","  ","&nbsp;"),array("","","","",""),$title);
						preg_match($array1['cate_name'],$t,$category);
						$cates=$array1['combine'].$t;
						$parents=$array1['parents'].','.$category[2];
						echo $cates." ".$category[2]." ".$parent."||".$parents." ".$title."\n";
						$w->write_cate($db,$shop,$category[2],$parent,$title);
						$w->write_endcate($db,$shop,$cates,$category[2],$parent,$parents);
					}
				}
				else{
					preg_match($array1['cate_name'],$cate,$category);
					$w->write_endcate($db,$shop,$cate,$category[2],$parent,$array1['parents']);
				}
				return;
			}
			else{
				if(!empty($temp[$index])){
					$i=0;
					foreach($temp[$index] as $t){
						$title = $temp_title[2][$i++];
						$title = strip_tags($title); //去除HTML標籤
						$title = str_replace(array("\r\n","\n","\t","  ","&nbsp;"),array("","","","",""),$title);
						preg_match($array1['cate_name'],$t,$category);
						$cates=$array1['combine'].$t;
						$array2['parents']=$array1['parents'].','.$category[2];
						echo $cates." ".$category[2]." ".$parent."||".$array2['parents']." ".$title."\n";
						$w->write_cate($db,$shop,$category[2],$parent,$title);
						$crawl->Digging($db,$shop,$cates,$category[2],$array2,$array);
					}
				}
				else{
					$crawl->Digging($db,$shop,$cate,$parent,$array2,$array);
				}
			}
		}

		public function Digging_level1($db,$shop,$cate,$parent,$array1,$array){
			$w=new WriteDb;
			$curl=new Curl;
			$data=$curl->SingleCurl($cate);

			if(!empty($array1['range'])){
				preg_match($array1['range'],$data,$data1);
				preg_match_all($array1['pattern'],$data1[1],$temp);
				preg_match_all($array1['title_pattern'],$data1[1],$temp_title);
			}
			else{
				preg_match_all($array1['pattern'],$data,$temp);
				preg_match_all($array1['title_pattern'],$data,$temp_title);
			}

			if($temp[2]==""){
				$index=1;
			}
			else $index=2;
			$i=0;
			if(!empty($temp[$index])){
				foreach($temp[$index] as $t){
					$title = $temp_title[2][$i++];
					$title = strip_tags($title); //去除HTML標籤
					$title = str_replace(array("\r\n","\n","\t","  ","&nbsp;"),array("","","","",""),$title);
					preg_match($array1['cate_name'],$t,$category);
					$cates=$array1['combine'].$t;
					echo $cates." ".$parent." ".$title."\n";
					$w->write_cate_level1($db,$shop,$cates,$category[2]);
					$w->write_cate($db,$shop,$category[2],$parent,$title);
				}
			}
		}
	}
?>